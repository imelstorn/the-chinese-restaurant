$(document).ready(function() {
	
	var header = $(".b-header");
	var heightTopBar = $(".b-top-bar").height();
	var classMenuBar = "jsMenuBar";
	var classMenuBarFix = "b-menu-bar_fixed";


	/*check position b-menu-bar on screen*/
	function checkMenuBar() {
		($(document).scrollTop() >= heightTopBar) ? $("."+ classMenuBar).addClass(classMenuBarFix) : $("."+ classMenuBar).removeClass(classMenuBarFix);
	}

	function checkHeaderImageHeight() {
		$(".jsHeader").height($(window).height() + "px");
		$(".jsHeaderImages").height($(window).height() + "px");
		$(".jsHeaderText").height($(window).height() + "px");
	}



	checkMenuBar();
	checkHeaderImageHeight();

	$(window).scroll(function() {
		checkMenuBar();
		checkHeaderImageHeight();
	});

	$(window).resize(function() {
		checkMenuBar();
		checkHeaderImageHeight();
	});

	$(window).on("orientationchange", false, function() {
		checkMenuBar();
		checkHeaderImageHeight();
	});	

	





});