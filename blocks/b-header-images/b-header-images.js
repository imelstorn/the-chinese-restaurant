$(function(){
	var ie = (/MSIE ([0-9]+)/.test(navigator.userAgent) && (new Number(RegExp.$1)) < 9);

	$(".b-header-images__choose-group__list").on("click", ".b-header-images__choose-group__item", function(){
		if (ie)
			$(".b-header-images__list").animate({left: $(this).index() * -1 * $(".b-header-images__item").width()});
		else
			$(".b-header-images__list").css("transform", "translateX(" + $(this).index() * -1 * $(".b-header-images__item").width() + "px)");
		$(".b-header-images__choose-group__item").removeClass("b-header-images__choose-group__item_active");
		$(this).addClass("b-header-images__choose-group__item_active");
	});

	/*$('.b-header-images__left-arrow').on('click', function(){
		var idx = $('.b-header-images__choose-group__item_active').index();
		idx = (idx == 0) ? $(".b-header-images__item").length - 1 : idx - 1;
		$(".b-header-images__choose-group__item").eq(idx).click();
	});

	$('.b-header-images__right-arrow').on('click', function(){
		var idx = $('.b-header-images__choose-group__item_active').index();
		idx = (idx == $(".b-header-images__item").length - 1) ? 0 : idx + 1;
		$(".b-header-images__choose-group__item").eq(idx).click();
	});*/

	function updateContainerWidth() {
		var images = $(".b-header-images__item");
		$(".b-header-images__item").width($(window).width());
		$(".b-header-images__item").height($(window).height());
		$(".b-header-images__list").css("width", images.length * $(".b-header-images").width() + 100);
		$(".b-header-images__choose-group__item").eq($('.b-header-images__choose-group__item_active').index()).click();


	}

	$(window).on('resize', updateContainerWidth);
	updateContainerWidth();
});
