$(function(){
	var t;

	function closeMobileMenu() {
		$(".p-body").css("position","relative");

		$(".p-body").animate({right:0, left: 0}, 300, function() {
			$(".b-mobile-menu").css("display","none");
			$(".p-body").unbind("click",closeMobileMenu);
			t = false;
		});
	}

	$(window).on("orientationchange", false, function() {
		closeMobileMenu();
	});

	$(document).delegate(".js-menuicon-top__item_more", "click", function(e) {

		e.preventDefault();
		
			if (t != true){
				$(".p-body").css({position: "fixed"});
				$(".b-mobile-menu").css("display", "block");
				$(".p-body").animate({right: "82%", left: "-82%"}, 300, function() {
					t = true;
					$(".p-body").bind("click", closeMobileMenu);
				});
			} else {
				closeMobileMenu();
			}
		
		return false;
	});
});
